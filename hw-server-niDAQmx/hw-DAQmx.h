#ifndef __hwDAQmx_H
#define __hwDAQmx_H

#define DBUF_LEN	0xffff  //65535 points max!
#define TIM_H		5e-7

#define HW_TRIGGER_PIN_1PPS "/Dev1/PFI14"
#define HW_TRIGGER_PIN_50Hz "/Dev1/PFI12"
#define HW_TRIGER_PFI       "PFI9"        //has to be on Dev1; fortunately there is only one Dev in SRET!
#define HW_TRIGER_DISC_TERM "/dev1/PFI9"  //for disconnect, somehow also device name is needed....

#define GLOBAL_START_PIN    "/Dev1/RTSI7"


struct cards_data {
	double	aoS[2][32];	     // static analog data
	double	aiD[2][DBUF_LEN];    // dynamic analog data (channel samples are interleaved)
	double	timing[2][DBUF_LEN]; // timing data in 's'
	double	cycle_time;	     // cycle time here is used as a 'timeout'; i.e. all the tasks shall be done earlier, we use this value as the timeout
    // measured data -- i.e. data OUT of the card:
    double  St_A;            // sampling time for sigA
    int     Ns_A;            // number of samples for sigA
	double	sigA[DBUF_LEN];  // data of the oscillogram
};

struct cards_data * new_data;		// these are the data, where the 'tcp' thread is allowed to write	
struct cards_data * hw_data;		// these are the data, that has to be 'correct' since they are read by the HW thread while running


int hw_init(char *errBuff);
int hw_reset(char *);
int hw_force_stop(char *);
int hw_safeOutput(char *);
int hw_trigger_delay_timer(double delay, int trig_source);
int hw_run(char *);

void hw_initialize_data_struct(struct cards_data *);
void getMeasDataStr(char *msg_in);
#endif
