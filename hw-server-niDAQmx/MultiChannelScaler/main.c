#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <errno.h> 
#include <NIDAQmx.h>

/*****
ATTENTION! this is an attempt to make this multithreadid by design!
1. thread - main
2. thread - blinkny (does on of LED)
3. counter1 is a 'sample clock generator' for counter0
4. counter0 is a edge counter, sampled by the sample_clok on RTSI6 (provided by ctr1)

UNDOCUMENTED functions are:
DAQmxSetCOPulseTerm      -- for setting the output pin of the counter
DAQmxSetCICountEdgesTerm -- for setting the pin on which the pulse train is sampled

OPEN PROBLEMS:
The counter 0 does not reset the value!!! I do not know, how to do this 
automatically after every 'n' points...    (thought there has to be a function etc..)
THEREFOR, the workaround now is 'removeAccumulation' which simply returns the 
difference between each 2 ctr0 'snapshots'.

Here I learned, ni-daqmx is multithreaded by design (every task is one thread):
https://forums.ni.com/t5/Measurement-Studio-for-VC/DAQmx-how-does-it-multithread/td-p/221953
*****/


/*********************************************************************
*
* ANSI C Example program:
*    ContAcq-IntClk.c
*
* Example Category:
*    AI
*
* Description:
*    This example demonstrates how to acquire a continuous amount of
*    data using the DAQ device's internal clock.
*
* Instructions for Running:
*    1. Select the physical channel to correspond to where your
*       signal is input on the DAQ device.
*    2. Enter the minimum and maximum voltage range.
*    Note: For better accuracy try to match the input range to the
*          expected voltage level of the measured signal.
*    3. Set the rate of the acquisition. Also set the Samples per
*       Channel control. This will determine how many samples are
*       read at a time. This also determines how many points are
*       plotted on the graph each time.
*    Note: The rate should be at least twice as fast as the maximum
*          frequency component of the signal being acquired.
*
* Steps:
*    1. Create a task.
*    2. Create an analog input voltage channel.
*    3. Set the rate for the sample clock. Additionally, define the
*       sample mode to be continuous.
*    4. Call the Start function to start the acquistion.
*    5. Read the data in the EveryNCallback function until the stop
*       button is pressed or an error occurs.
*    6. Call the Clear Task function to clear the task.
*    7. Display an error if any.
*
* I/O Connections Overview:
*    Make sure your signal input terminal matches the Physical
*    Channel I/O control. For further connection information, refer
*    to your hardware reference manual.
*
*********************************************************************/

#define DATA_LEN 1000

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else

//int32 CVICALLBACK EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData);


int out_print(float sampling_period, uInt32 *data, int removeAccumulation){
    // print data in the format for stdio_show.py
    int i;
    uInt32 last=0;

    printf("PLT:%f:", sampling_period);
    for(i=0; i<DATA_LEN; i++){
        if(removeAccumulation){
            if(i!=0) printf("%d,", data[i]-last);
            last = data[i];
        }else{
            printf("%d,", data[i]);
        }
    }
    printf(":END\n");
    return(0);
}


void *blinky_thread(void * params){
	int32       error=0;
	TaskHandle  taskHandle=0;
	char        errBuff[2048]={'\0'};
    int do_print = 0;

    uInt8       data[1]={0};

    if(((char *) params)[0] != '0') do_print=1;

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
	DAQmxErrChk (DAQmxCreateTask("Blinky", &taskHandle));
    DAQmxErrChk (DAQmxCreateDOChan(taskHandle,"Dev1/port2/line1","",DAQmx_Val_ChanForAllLines));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));


    while(1){
        /*********************************************/
        // DAQmx Write Code
        /*********************************************/
        DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle, 1 , 1, 10.0, DAQmx_Val_GroupByChannel, data, NULL, NULL));
        if(data[0] == 0) data[0] = 1;
        else data[0] = 0;
        if(do_print) printf("\nBlinky t: Setting the LED to : %d\n", data[0]);
        sleep(1);
    }

Error:
	if( DAQmxFailed(error) )
		DAQmxGetExtendedErrorInfo(errBuff,2048);
	if( taskHandle!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
	}
	if( DAQmxFailed(error) )
		printf("Blinky t: DAQmx Error in blinky_thread : %s\n",errBuff);
    return(NULL);
}


int run_sample_clock_counter(float sampling_period){
    int         error=0;
    TaskHandle  taskHandle=0;
    char        errBuff[2048]={'\0'};

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
    DAQmxErrChk (DAQmxCreateCOPulseChanFreq(taskHandle,"Dev1/ctr1","",DAQmx_Val_Hz,DAQmx_Val_Low,0.0,1.00/sampling_period,0.50));
    DAQmxErrChk (DAQmxCfgImplicitTiming(taskHandle,DAQmx_Val_ContSamps,1000));

    DAQmxErrChk (DAQmxRegisterDoneEvent(taskHandle,0,DoneCallback,NULL));

    DAQmxErrChk (DAQmxSetCOPulseTerm(taskHandle, "Dev1/ctr1", "PFI13"));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));

    printf("Generating pulse train as sample clock on counter1...\n");
    return(0);

Error:
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("End of program, press Enter key to quit\n");
    getchar();
    return 0;
}



int main(int argc, char **argv){
	int32       error=0;
	TaskHandle  taskHandle=0;
    int32       read;
    uInt32      data[DATA_LEN];
	char        errBuff[2048]={'\0'};

    int removeAccumulation=1;

    float       sampling_period=1e-3;   //this sahll be in seconds

    pthread_t thread_blinky;
    char *params="0 - Bla bla bla"; // If the first character is '0' blinky thread will not print anything! Otherwise it will

    pthread_create(&thread_blinky, NULL, blinky_thread, (void *) params );




/////////////////////////////////  Initialize counter1 as counter for "sampling period/frequency"

    run_sample_clock_counter(sampling_period);

    DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr1InternalOutput", "/Dev1/RTSI6", DAQmx_Val_DoNotInvertPolarity));


///////////////////////////////// Initialize and read counter0 as 'event-counter-buffered' (i.e. our MCS)


    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
    DAQmxErrChk (DAQmxCreateCICountEdgesChan(taskHandle,"Dev1/ctr0","",DAQmx_Val_Rising,0,DAQmx_Val_CountUp));
    DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle,"/Dev1/RTSI6",1000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000));

    DAQmxErrChk (DAQmxSetCICountEdgesTerm(taskHandle, "/Dev1/Ctr0", "/Dev1/PFI8"));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));

    printf("Continuously reading. Press Ctrl+C to interrupt\n");
    while( 1 ) {
        /*********************************************/
        // DAQmx Read Code
        /*********************************************/
        DAQmxErrChk (DAQmxReadCounterU32(taskHandle,1000,10.0,data,1000,&read,NULL));
        
        out_print(sampling_period, data, removeAccumulation);
        //printf("\rAcquired %d samples",(int)read);
        //fflush(stdout);
    }

Error:
    puts("");
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("End of program, press Enter key to quit\n");
    getchar();
    return 0;
	}




int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData)
{
    int32   error=0;
    char    errBuff[2048]={'\0'};

    // Check to see if an error stopped the task.
    DAQmxErrChk (status);

Error:
    if( DAQmxFailed(error) ) {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        DAQmxClearTask(taskHandle);
        printf("DAQmx Error: %s\n",errBuff);
    }
    return 0;
}

