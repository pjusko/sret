//* Copyright 2020 Pavol Jusko pjusko@mpe.mpg.de; see LICENSE.txt
#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <pthread.h> 

//#include <pthread.h>

#define MSG_MAX 8096 
#define PORT 8000 
#define SA struct sockaddr 


int tcpCommandParser(int sockfd){ 
    char buff[MSG_MAX]; 
    int n, len; 

    // infinite loop for tcp incoming messages 
    for (;;){ 
        bzero(buff, MSG_MAX); 
  
        len = read(sockfd, buff, sizeof(buff)) ; 

        if (len <= 0) return(len);  //ERROR somewhere!!!!

        buff[len] = '\0';   //make it a string!


        printf("R-msg: %s\n", buff); 
  
        write(sockfd, buff, sizeof(buff)); 
  
        // if msg contains "Exit" then server exit and chat ended. 
        if (strncmp("exit", buff, 4) == 0){ 
            printf("Server Exit...\n"); 
            break; 
        } 
    } 
} 

void *tcp_thread(void *params){
    int sockfd, connfd, addr_size;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        perror("Socket initialisation failed");
        return(NULL);
    }
    bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);


    int enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
        perror("setsockopt(SO_REUSEADDR) failed");
        return(NULL);
    }
    
    // bind to the socekt
    if ((bind(sockfd, (struct sockaddr *) &servaddr, sizeof(struct sockaddr_in))) != 0){
        perror("Socket bind failed");
        return(NULL);
    }

    // now listen on the socket
    if ((listen(sockfd, 5)) != 0){ 
        perror("Listen failed");
        return(NULL);
    } 
    
    // Accept the connection
    for (;;){
        printf("Server is listening...\n");
        addr_size = sizeof(cli);
        connfd = accept(sockfd, (struct sockaddr *) &cli, &addr_size);
        if (connfd < 0){
            perror("Server acccept failed"); 
            return(NULL);
        } else printf("We have a client...\n");


        tcpCommandParser(connfd);
        close(connfd);
    }

    close(sockfd);
}

int main(int argc, char **argv){
    pthread_t thread_tcp;
    char *params="Bla bla bla";
    int i;

    pthread_create(&thread_tcp, NULL, tcp_thread, (void *) params);

    while(1){
        printf("Main still alive: %d\n", i);
        i++;
        sleep(1);
    }


    return(EXIT_SUCCESS);
} 

