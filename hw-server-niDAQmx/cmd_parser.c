/* Parser of the commands received on the TCP connection
 * 
 * Copyright 2020 Pavol Jusko pjusko@mpe.mpg.de; see LICENSE.txt
 */
#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <errno.h> 
#include "main.h" 
#include "hw-DAQmx.h" 


/* print the content of new_data into *msg in a nicely readable way */
size_t str_new_data(char *msg){
    char *start=msg;
    int Dev, i;

    msg += sprintf(msg, "new_data:\n");
    msg += sprintf(msg, "    cyc: %f\n", new_data->cycle_time);
    msg += sprintf(msg, "    SigA: St=%f Ns=%d\n", new_data->St_A, new_data->Ns_A);
    //msg += sprintf(msg, "    samples1: %d samples2: %d\n", new_data->num_samples[0], new_data->num_samples[1]);
    //for(Dev=0;Dev<2;Dev++){
    //    msg += sprintf(msg, "   aoS[%d] = ", Dev);
    //    for(i=0;i<32;i++) msg += sprintf(msg, " %g,", new_data->aoS[Dev][i]);
    //    msg += sprintf(msg, "\n   aoD[%d] = ", Dev);
    //    for(i=0;i<new_data->num_samples[Dev]*8;i++) msg += sprintf(msg, " %g,", new_data->aoD[Dev][i]);
    //    msg += sprintf(msg, "\n   tim[%d] = ", Dev);
    //    for(i=0;i<new_data->num_samples[Dev];i++) msg += sprintf(msg, " %g,", new_data->timing[Dev][i]);
    //    msg += sprintf(msg, "\n   -------------------\n");
    //}
    //*(msg - 1) = '\0';  //delete the last trailing neline!
    return(msg - start);
}


/* Parses the string into double array.
 * ends after first 'problem' and returns the number of conversions into the array 
 * string can be \0 terminated or ',' or ';' terminated, all will work!
 */
int arr_from_msg(char *msg, double *arr){
    int i=0, chars;
    double num;

    while(sscanf(msg, "%lg%n", &num, &chars)==1){
        arr[i++] = num;
        msg += chars + 1;
    }
    return(i);
}

/* Parser for the S key=val command
 * returns 0 on success.
 */
int parse_S_cmd(char *cmd){
    char *eq_pos;
    char key[20], val[20];
    double local_d;
    int local_i;

    if(cmd[1] != ' ') return(-1);
    if((eq_pos = strchr(cmd, '=')) == NULL) return(-1);

    strcpy(val, eq_pos+1);
    *eq_pos = '\0';
    strcpy(key, cmd+2);

    // HERE pair key: val (both strings!) are available
    //printf("%s : %s\n", key, val);

    if(strstr(key, "St_A")){
        sscanf(val, "%lg", &local_d);
        if((local_d > 0) && (local_d < 1)){
            new_data->St_A = local_d;
            return(0);
        }else{
            return(-2); //Means wrong key!!!
        }
    }
    if(strstr(key, "Ns_A")){
        sscanf(val, "%d", &local_i);
        if(local_i <= DBUF_LEN){
            new_data->Ns_A = local_i;
            return(0);
        }else{
            return(-2); //Means wrong key!!!
        }
    }

    return(-1);
}


/* Parse the command. This command is already a \0 terminated string withouth the ';' at the end!
 * returns <0 on error and *res contains a message.
 * 0 - sucecess; 1 - start the RUN; 2 - close TCP connection
 */
int parse_message(char *cmd, char *res){
    int len, i, dev_i, local, conversions, Dev=-1;
    double *dptr, val, localD;
    char key[20], localS[20];
    res[0] = '\0';
  
    len = strlen(cmd);
    strcpy(res, "Ok.\n");
    //printf("cmd: %s; len: %ld\n", cmd, strlen(cmd));

    switch(cmd[0]){
        case 'Q':
            // shall return the current state;
            i = pthread_mutex_trylock(&hw_running_mutex);
            if(i == 0){
                sprintf(res, "Ok. Not running.\n");
                pthread_mutex_unlock(&hw_running_mutex);
                return(0);
            }
            if(i == EBUSY){ 
                sprintf(res, "Ok. Yes running %d of %d.\n", hw_curr_run, hw_runs);
                return(0);
            }
            sprintf(res, "Ee: Mutex LOCK ERROR Q.\n");
            return(-1);
        case 'R': 
            if(sscanf(cmd, "R %d", &local)!=1){
                strcpy(res, "Ee: not a valid RUNS value.\n");
                printf(res);
                return(-1);
            }

            i = pthread_mutex_trylock(&hw_running_mutex);
            if(i == 0){         // yes, we can start the next run
                hw_runs = local;
                alreadyRunning=0;                                   // this is just for 'reporting' purposes when printing!
                memcpy(hw_data, new_data, sizeof(struct cards_data));   // copy data to the hw regeneration buffer:
                /////////new_data->static_changed=0;                             // reset the new STATIC data flag!

                // start IT now!
                pthread_cond_signal(&start_cond);
                pthread_mutex_unlock(&hw_running_mutex);
                strcpy(res, "Ok. Starting RUN.\n");     //ATTENTION: 'data' is returned from the hw thread!!!
                return(1);
            }
            if(i == EBUSY){ 
                if(alreadyRunning){
                    printf("\rcmd: Already running: %d!", alreadyRunning);
                }else{    
                    printf("cmd: Already running!\n");
                }
                alreadyRunning++;
                strcpy(res, "Ee: hw already running.\n");
                return(0);
            }
            sprintf(res, "Ee: Mutex LOCK ERROR R.\n");
            return(-1);
        case 'C': 
            if(sscanf(cmd, "C %lg", &(new_data->cycle_time))==1) return(0);
            strcpy(res, "Ee: not a valid CYCLE_TIME time value.\n");
            printf(res);
            return(-1);
        case 'P': 
            str_new_data(res);
            return(0);
        case 'L': 
            if(sscanf(cmd, "L %lg %d", &localD, &local)!=2){
                strcpy(res, "Ee: not a valid TRIGGER DELAY time value.\n");
                printf(res);
                return(-1);
            }
            i = pthread_mutex_trylock(&hw_running_mutex);
            if(i == 0){         // yes, we can update the trigger delay value in the CNTR!
                hw_trigger_delay_timer(localD, local);
                pthread_mutex_unlock(&hw_running_mutex);
                return(0);
            }
            if(i == EBUSY){ 
                strcpy(res, "Ee: hw already running.\n");
                return(0);
            }
            sprintf(res, "Ee: Mutex LOCK ERROR V.\n");
            return(-1);
        case 'A':
        case 'D':
        case 'Z':
            return(0);
        case 'S':
            if(cmd[1] == ' ') i = parse_S_cmd(cmd);
            if(i == 0) return(0);
            if(i == -2) strcpy(res, "Ee: wrong val for the key.\n");
            else strcpy(res, "Ee: not a valid S (set) command or key = val pair.\n");
            return(-1);
        case 'V':
            i = pthread_mutex_trylock(&hw_running_mutex);
            if(i == 0){         // yes, we can restart not running
                hw_reset(errBuff);
                hw_init(errBuff);
                pthread_mutex_unlock(&hw_running_mutex);
                return(0);
            }
            if(i == EBUSY){ 
                strcpy(res, "Ee: hw already running.\n");
                printf(res);
                return(0);
            }
            sprintf(res, "Ee: Mutex LOCK ERROR V.\n");
            return(-1);
        case 'X':
            hw_runs = 0;
            if((cmd[1] == ' ') && (cmd[2] == 'F')){
                strcpy(res, "Ok. Forcefull HW STOP\n");
                //and stop all the hardware FORCEFULLY!
                hw_force_stop(errBuff);
                hw_safeOutput(errBuff);
            }
            return(0);
        case 'N':
            strcpy(res, "Last HW ERR: ");
            strcat(res, errBuff);
            strcat(res, "\n");
            return(0);
        case 'E':
            // shall close the connection
            return(2);
    }
    strcpy(res, "Ee: unknown command.\n");
    return(-1);
}

