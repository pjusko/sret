#ifndef __MAIN_H
#define __MAIN_H

char * errBuff; 					// hw errors are stored here


volatile int hw_runs;			    // <=0 hw is not running;  >0 hw is running!
volatile int hw_curr_run;		    // current iteration
volatile int alreadyRunning;        // used to not display 'cmd: Already running!' many many times!

volatile int sockfd;                // tcp socket (to be used in run to return data)

pthread_mutex_t hw_running_mutex;	// mutex to chek if hw is running
pthread_cond_t  start_cond;         //  = PTHREAD_COND_INITIALIZER;
pthread_cond_t  done_cond;          //  = PTHREAD_COND_INITIALIZER;

#endif
