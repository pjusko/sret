// Copyright 2020 Pavol Jusko pjusko@mpe.mpg.de; see LICENSE.txt
#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <errno.h> 
#include "main.h" 
#include "cmd_parser.h" 
#include "hw-DAQmx.h" 


#define MSG_MAX 0xfffff //1Mbyte
#define PORT 10700 
#define SA struct sockaddr 

char r_msg[MSG_MAX], w_msg[MSG_MAX];

void *hw_thread_function(){
    printf("HW_T: new HW thread started\n");
    printf("HW_T: sockfd : %d\n", sockfd);

    hw_curr_run = 0;        //this is volatile, but only written in this function!
    hw_runs = 0;
    for(;;){
        pthread_mutex_lock(&hw_running_mutex);
        while(hw_runs<=0){
            pthread_cond_wait( &start_cond, &hw_running_mutex );
        }
        //XXX printf("HW_T: new 'start' condition with hw_runs= %d\n", hw_runs);
        strcpy(errBuff, "None\n");

        for(hw_curr_run=0;hw_curr_run<hw_runs;hw_curr_run++){
            printf("HW_T: run %d of %d\n", hw_curr_run, hw_runs);
            hw_run(errBuff);
            //if you want cmd 'E' to work also in RUN pthread_testcancel(); seems to work anyway!!!
        }

        hw_curr_run = 0;
        hw_runs = 0;
        
        //triger the sending of the measured data in the main thread!
        pthread_cond_signal(&done_cond);
        pthread_mutex_unlock(&hw_running_mutex);
    }

    //hw_safeOutput(errBuff);
    printf("HW_T: thread terminating itself. this should never happen!\n");
}


int tcpCommandParser(int sockfd){ 
    char *r_ptr = r_msg, *c_ptr;        //r_ptr - recieve pointer; c_ptr - next comd pointer;
    int r_len, w_len, ret, occ_len=0;   //occ_len is the 'occupied' length in r_msg in case only partial message received! 

    pthread_t hw_thread;

    strcpy(w_msg, "Hello! I am SRET CMD. Ver=0.1\n");
    w_len = strlen(w_msg);
    write(sockfd, w_msg, w_len);

    pthread_create( &hw_thread, NULL, &hw_thread_function, NULL);

    // infinite loop for tcp incoming messages 
    for (;;){ 
        r_len = read(sockfd, r_ptr, sizeof(r_msg) - occ_len) ; 

        if (r_len <= 0){
            printf("SOCKET read UNBLOCKED IN MAIN THREAD\n");
            usleep(100000);
            return(r_len);  //ERROR somewhere!!!!
        }

        r_ptr[r_len] = '\0';   //make it a string!

        r_ptr = r_msg;  //even if partial message has been recieved, we have to treat it from the beginning)
        //printf("R-msg: LEN:%ld\n%s", strlen(r_ptr), r_ptr); 

        for(;;){
            c_ptr = strsep(&r_ptr, ";\n\r");  //the string there can contain more characters to be used as delimiter!!!
            if(r_ptr == NULL){           
                // rewind r_ptr to the begginig of allocated space if buf is empty
                // OTHERWISE copy remaining buf to beginning and put r_ptr at the end of copied data
                occ_len = strlen(c_ptr);
                if(occ_len==0){
                    r_ptr = r_msg;
                }else{ 
                    strcpy(r_msg, c_ptr);
                    r_ptr = r_msg + occ_len; 
                }
                break;
            }
            if(strlen(c_ptr) == 0) continue;     //this is here for \n and ;; and such!
            ret = parse_message(c_ptr, w_msg);
            //UNCOMENT for verobse output on TCP in/out
            //printf("-I: %s; R: %d -O: %s", c_ptr, ret, w_msg);
            w_len = strlen(w_msg);
            write(sockfd, w_msg, w_len);

            if(ret==1){                         //started the run! we have to wait until the data is ready and send it back over TCP!!!!
                pthread_mutex_lock(&hw_running_mutex);
                while(hw_runs > 0){
                    pthread_cond_wait( &done_cond, &hw_running_mutex );
                }
                pthread_mutex_unlock(&hw_running_mutex);

                // send back the measured data
                getMeasDataStr(w_msg);
                //strcpy(w_msg, "RUN is done and I shall send data.\n");
                w_len = strlen(w_msg);
                write(sockfd, w_msg, w_len);

                continue;
            }
            if(ret==2){
                printf("Server Exit...\n"); 
                goto Ok_exit;
            }
        }
    }

Ok_exit:
    pthread_cancel(hw_thread);
    pthread_join(hw_thread, NULL);
    //HACK (OR I DO NOT KNOW):
    //check if the hw_running_mutex stayed locked or not!!!
    if(pthread_mutex_trylock(&hw_running_mutex) == EBUSY){
        printf("Unlocking hw_r_mutex\n");
        pthread_mutex_unlock(&hw_running_mutex);
    }
    printf("Thread joined.\n"); 
    return(0);
} 


int main(int argc, char **argv){
    int connfd, test=0;
    socklen_t addr_size;
    struct sockaddr_in servaddr, cli;
    int opt;

    new_data = (struct cards_data *) malloc(sizeof(struct cards_data));
    hw_data = (struct cards_data *) malloc(sizeof(struct cards_data));
    errBuff = (char *) malloc(2048*sizeof(char));
    errBuff[0] = '\0';

    hw_initialize_data_struct(new_data);


    while((opt = getopt(argc, argv, "Fh")) != -1){  
        switch(opt){
            case 'h':
                printf("Usage: hw-server [ -F ]\n\n");
                printf("Options: \n");
                printf("\tF - DOES NOT WORK ANYMORE\n");
                return(EXIT_SUCCESS);
            case '?':
                printf("Unknown option: %c\n", optopt);
                return(EXIT_FAILURE);
        }
    }

    /************************************************* TCP connection **********************/
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1){
        perror("Socket initialisation failed");
        return(EXIT_FAILURE);
    }
    explicit_bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);


    int enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0){
        perror("setsockopt(SO_REUSEADDR) failed");
        return(EXIT_FAILURE);
    }
    
    // bind to the socekt
    if ((bind(sockfd, (struct sockaddr *) &servaddr, sizeof(struct sockaddr_in))) != 0){
        perror("Socket bind failed");
        return(EXIT_FAILURE);
    }

    // now listen on the socket
    if ((listen(sockfd, 1)) != 0){ 
        perror("Listen failed");
        return(EXIT_FAILURE);
    } 
    
    // Accept the connection
    for (;;){
        printf("Server is listening...\n");
        addr_size = sizeof(cli);
        connfd = accept(sockfd, (struct sockaddr *) &cli, &addr_size);
        if (connfd < 0){
            perror("Server acccept failed"); 
            return(EXIT_FAILURE);
        } else printf("We have a client...\n");

        //hw_reset(errBuff);
        //hw_init(errBuff);

        tcpCommandParser(connfd);
        close(connfd);

        //hw_reset(errBuff);
    }

    close(sockfd);
    return(EXIT_SUCCESS);
} 

