/*
 * This is the file, where all the communication with ni-DAQmx takes place
 *
 * there are 3 defines:
 * 	NO_DAQmx -- will kick out all the caals to DAQmx 
 * 	(usefull if You do not have it installed and want to test)
 *
 * 	SELF_TEST_MAIN -- if You want to have a small main() to run a test...
 *
 *  TIMING_REPORTING -- will print timing information for each 'run' cycle 
 *
 * compile as:
 * cc -D_POSIX_C_SOURCE=200809L -L/usr/lib/x86_64-linux-gnu -DSELF_TEST_MAIN -lm -lnidaqmx -o hw hw-DAQmx.c
 *
 * Copyright 2020 Pavol Jusko pjusko@mpe.mpg.de; see LICENSE.txt
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#ifndef NO_DAQmx
#include <NIDAQmx.h>
#endif //NO_DAQmx
#include "hw-DAQmx.h"
#include "main.h"


#ifndef NO_DAQmx
//int hw_not_pretend;   only if set to !=0 will actual call to the hardware happen
//#define DAQmxErrChk(functionCall) if( hw_not_pretend && DAQmxFailed(error=(functionCall)) ) goto Error; else
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
int CVICALLBACK hw_DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData);
#else   //NO_DAQmx
#define DAQmxErrChk(functionCall)
#define DAQmxGetExtendedErrorInfo(errBuff, val)
#define DAQmxFailed(error) 0
#define DAQmxStopTask(val) 
#define DAQmxClearTask(val) 
#define CVICALLBACK
#define TaskHandle int
#endif  //NO_DAQmx


//#define TIMING_REPORTING

#ifdef TIMING_REPORTING
#include <time.h>
double timing_statistics[3]={1000, 0, 0};
int timing_n=0;
struct timespec ttriger;
volatile int update_ttrigger;
#endif //TIMING_REPORTING


struct TaskSettings {
    TaskHandle handle;
    char name[32];
    char devStr[10];
    char settingsStr[128];
    char triggerStr[32];
    char ctrStr[32];
};

#define MAX_DEV 2
enum TaskHandlesEnum {eAOStatic, eAODynamic, eClk, eMaxHandle};    //MaxHandle gives us the number of elements in the enum!
char TaskHandleNames[3][20] = {"AOSta", "AODyn", "Clk"};
TaskHandle  THs[MAX_DEV][eMaxHandle];
struct TaskSettings tasks[MAX_DEV][eMaxHandle];

double timingH[DBUF_LEN];

TaskHandle trigTimerHandle=0, mcsHandle=0;


/* Inits the internals of the hardware. This function can be only called after program start, or after hw_reset!!
 * otherwise You'll get errors! */
int hw_init(char *errBuff){
    int i, j, error=0;
    struct TaskSettings *task;

    trigTimerHandle=0;
    mcsHandle=0;

    /*for(i=0;i<MAX_DEV;i++)
        for(j=0;j<eMaxHandle;j++){ 
            task=&tasks[i][j];
            task->handle = 0;
            sprintf(task->devStr, "Dev%d", i+1);
            sprintf(task->name, "D%d-%s", i+1, TaskHandleNames[j]);
            task->ctrStr[0] = '\0';
            //unfortunately the rest has to be done almost manually:
            switch(j){
                case eAOStatic:
                    task->triggerStr[0] = '\0';
                    sprintf(task->settingsStr, "Dev%d/ao0:31", i+1);
                    break;
                case eAODynamic:
                    switch(i){
                        case 0:
                            strcpy(task->triggerStr, "/Dev1/RTSI2");
                            strcpy(task->settingsStr, "Dev1/ao0,Dev1/ao4,Dev1/ao9,Dev1/ao14,Dev1/ao16,Dev1/ao20,Dev1/ao26,Dev1/ao28");
                            //strcpy(task->settingsStr, "Dev1/ao0,Dev1/ao4");
                            break;
                        case 1:
                            strcpy(task->triggerStr, "/Dev2/RTSI5");
                            strcpy(task->settingsStr, "Dev2/ao1,Dev2/ao4,Dev2/ao10,Dev2/ao12,Dev2/ao17,Dev2/ao20,Dev2/ao24,Dev2/ao28");
                            break;
                    }
                    break;
                case eClk:
                    switch(i){
                        case 0:
                            strcpy(task->triggerStr, "/Dev1/RTSI1");
                            strcpy(task->settingsStr, "Dev1/Ctr1");
                            strcpy(task->ctrStr, "C1");
                            break;
                        case 1:
                            strcpy(task->triggerStr, "/Dev2/RTSI1");
                            strcpy(task->settingsStr, "Dev2/Ctr1");
                            strcpy(task->ctrStr, "C1");
                            break;
                    }
                    break;
            }
        }
     */

    /*printf("These are the HANDLES:\n");
    for(i=0;i<MAX_DEV;i++)
        for(j=0;j<eMaxHandle;j++){
            task=&tasks[i][j];
            printf("i:%d j:%d Name:%s Dev:%s Set:%s Trig:%s Ctr:%s\n", i, j, task->name, task->devStr, task->settingsStr, task->triggerStr, task->ctrStr);
        }*/


    // SEE NOTE AT THE END CONCERNING CNTR AND OUTPUTS!!!
    //this is done in hw_trigger_delay_timer()! DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr0InternalOutput", "/Dev1/PFI3", DAQmx_Val_DoNotInvertPolarity));  // GLOBAL TRIG
    //DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr0InternalOutput", "/Dev1/RTSI1", DAQmx_Val_DoNotInvertPolarity));  // GLOBAL TRIG
    //////DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr1InternalOutput", "/Dev1/RTSI2", DAQmx_Val_DoNotInvertPolarity));
    //////DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr2InternalOutput", "/Dev1/RTSI4", DAQmx_Val_DoNotInvertPolarity));
    //////if(MAX_DEV>1){
    //////    DAQmxErrChk (DAQmxConnectTerms("/Dev2/Ctr1InternalOutput", "/Dev2/RTSI5", DAQmx_Val_DoNotInvertPolarity));
    //////}

    return(0);

Error:
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    printf("hw: DAQmx Error: %s\n", errBuff);
    return(-1);
}


int hw_force_stop(char *errBuff){
    int i, j, error=0;

   // for(i=0;i<MAX_DEV;i++)
   //     for(j=0;j<eMaxHandle;j++){
   //         if( tasks[i][j].handle ){
   //             DAQmxErrChk (DAQmxStopTask(tasks[i][j].handle));
   //             DAQmxErrChk (DAQmxClearTask(tasks[i][j].handle));
   //         }
   //     }
    return(0);

Error:
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    printf("hw: DAQmx Error: %s\n", errBuff);
    return(-1);
}


int hw_reset(char *errBuff){
    struct TaskSettings *task;
    int dev_i, j, error=0;
    char devices[2][10] = {"Dev1", "Dev2"};

    printf("hw: hw_reset()\n");
    if(trigTimerHandle!=0){
        DAQmxErrChk (DAQmxStopTask(trigTimerHandle));
        DAQmxErrChk (DAQmxClearTask(trigTimerHandle));
        trigTimerHandle=0;
    }

    //DAQmxSelfTestDevice (const char deviceName[]); 
    return(0);

Error:
    DAQmxGetExtendedErrorInfo(errBuff, 2048);
    printf("hw: DAQmx Error: %s\n", errBuff);
    return(-1);
}


int hw_safeOutput(char *errBuff){
    int dev_i, error=0;
    /*struct TaskSettings *task;
    double dataStatic[32] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 
    
    for(dev_i=0;dev_i<MAX_DEV;dev_i++){
        task=&tasks[dev_i][eAOStatic];
        DAQmxErrChk (DAQmxCreateTask("AO-static-SAFE-OUTPUT", &(task->handle)));
        if(task->handle==0) return(-1);
        DAQmxErrChk (DAQmxCreateAOVoltageChan(task->handle, task->settingsStr, "", -10.0, 10.0, DAQmx_Val_Volts, NULL));
        DAQmxErrChk (DAQmxStartTask(task->handle));
        DAQmxErrChk (DAQmxWriteAnalogF64(task->handle, 1, 1, 1, DAQmx_Val_GroupByChannel, dataStatic, NULL, NULL));

        DAQmxErrChk (DAQmxStopTask(task->handle));
        DAQmxErrChk (DAQmxClearTask(task->handle));
        task->handle=0;
    }*/
    printf("hw: hw_safe_output()\n");

Error:
    if(DAQmxFailed(error)){
        DAQmxGetExtendedErrorInfo(errBuff, 2048);
        printf("hw: DAQmx Error: %s\n", errBuff);
        return(-1);
    } else return(0);
}

/* Callback function for the CTR0 timer, which functions as a trigger for everything
 */
int CVICALLBACK Ctr0OutputCallback(TaskHandle taskHandle, int signalID, void *callbackData){
/* I do not know how to easily get what is the value on the CounterOutput!
 * thus I just made the 'TICKS' such, that it is ON for almost the whole period
 * and OFF only for few us; thus when the second 'Output even' comes (falling edge)
 * it will get sampled here for timing purposes only after it has been processed in the hw_run()
 */
#ifdef TIMING_REPORTING
    // this callback is called after every 'trigger' event. When cycle time > 1s, we onlu want to
    // have the first trigger not all of them....
    if(update_ttrigger){
        clock_gettime(CLOCK_REALTIME, &ttriger);
        update_ttrigger=0;
    }
#endif //TIMING_REPORTING
    return(0);
}

/* Start/ update the CTR0 that is running as a main trigger for everything else.
 * delay parameter offsets the 'shot' from this timer with respect to the system trigger.
 *
 * also if delay is < 0; we just run it as a free running timer with rep. rate of 1s
 * NOTE: this function checks for the hw_running mutex!
 */
int hw_trigger_delay_timer(double delay, int trig_source){
    int error=0, freeRunning=0;
    char settingsStr[] = "Dev1/Ctr0";
    char ctrStr[] = "C0";
    char hw_trigger_pin_variable[64];
    char errBuff[2048];

    if(trig_source == 1) strcpy(hw_trigger_pin_variable, HW_TRIGGER_PIN_50Hz);
    else strcpy(hw_trigger_pin_variable, HW_TRIGGER_PIN_1PPS);

    if(trigTimerHandle!=0){
        DAQmxErrChk (DAQmxStopTask(trigTimerHandle));
        DAQmxErrChk (DAQmxClearTask(trigTimerHandle));
        trigTimerHandle=0;
    }

    DAQmxErrChk (DAQmxCreateTask("TRIG_C0", &trigTimerHandle));
    
    if(delay>=0){
        if(delay < 1e-6) delay = 1e-6;
        DAQmxErrChk (DAQmxCreateCOPulseChanTime(trigTimerHandle, settingsStr, ctrStr, DAQmx_Val_Seconds, 
            DAQmx_Val_Low, delay, 0.00001, 0.00001));   
        DAQmxErrChk (DAQmxCfgImplicitTiming(trigTimerHandle, DAQmx_Val_FiniteSamps, 1));

        DAQmxErrChk (DAQmxCfgDigEdgeStartTrig(trigTimerHandle, hw_trigger_pin_variable, DAQmx_Val_Rising));
        DAQmxErrChk (DAQmxSetStartTrigRetriggerable(trigTimerHandle, TRUE));
        DAQmxErrChk (DAQmxSetCOEnableInitialDelayOnRetrigger(trigTimerHandle, ctrStr, TRUE));
    }else{  //just an implicit 'delay' (e.g. 1s) rep rate free running timer
        delay *= -1.0;
        freeRunning=1;
        if(delay < 1e-6) delay = 1e-6;
        DAQmxErrChk (DAQmxCreateCOPulseChanTime(trigTimerHandle, settingsStr, ctrStr, DAQmx_Val_Seconds, 
            DAQmx_Val_Low, 0.000001, delay-1e-6, 1e-6));   
        DAQmxErrChk (DAQmxCfgImplicitTiming(trigTimerHandle, DAQmx_Val_ContSamps, 1));
    }

    // NOW we start everything at the same time!
    DAQmxErrChk (DAQmxConnectTerms(HW_TRIGER_DISC_TERM, GLOBAL_START_PIN, DAQmx_Val_DoNotInvertPolarity)); 

    //register call back (this is only for time reporting purposes!
#ifdef TIMING_REPORTING
    DAQmxErrChk (DAQmxRegisterSignalEvent(trigTimerHandle , DAQmx_Val_CounterOutputEvent, 0, Ctr0OutputCallback, NULL));
#endif
    DAQmxErrChk (DAQmxSetCOPulseTerm(trigTimerHandle, "Dev1/ctr0", HW_TRIGER_PFI));  
    DAQmxErrChk (DAQmxStartTask(trigTimerHandle)); 
    printf("hw: TRIG_C0 has been reset to %f delay. FreeRunning %d. Trig %d.\n", delay, freeRunning, trig_source);
    
Error:
    if(DAQmxFailed(error)){
        DAQmxGetExtendedErrorInfo(errBuff, 2048);
        printf("hw: trigger_delay_timer Error: %s\n", errBuff);
        return(-1);
    } else return(0);
}



/* CAREFULL, this is the only function, that shall be called from the hw_thread!
 * all the other functions shall be called from the main thread
 *
 * function blocks until the 'run' is complete, or timeout occured
 *
 * HOW is it all CONNECTED:
 *            trig     Dev1        out      
 *    -----------------------------------
 *    CTR0    PFIx     GLOB_TRIG   RTSI1    
 *    ******************************  none!!! CTR1    RTSI1    AO_CLK_1    RTSI2    
 */ 
int hw_run(char *errBuff){
    int dev_i, j, error=0, d_msg_len, ret_err;
    struct TaskSettings *task;
#ifdef TIMING_REPORTING
    struct timespec tstart, tend;
#endif

	TaskHandle  taskHandle=0;
	int         read;
	double      data[1000];


    printf("HW_RUN: now.\n");

#ifdef NO_DAQmx
    usleep((useconds_t) (hw_data->cycle_time * 1e6));
    return(0);
#endif //NO_DAQmx

    // triger for all the COUNTERS and DYNAMIC data has to be disconnected before starting to load stuff in the Devices!
    DAQmxErrChk (DAQmxDisconnectTerms(HW_TRIGER_DISC_TERM, GLOBAL_START_PIN)); 

//##### HERE IS THE RUN INITIALISATION    


    
    /*********************************************/
	// DAQmx Configure Code
	/*********************************************/
	DAQmxErrChk (DAQmxCreateTask("ao_sigA",&taskHandle));
    //There is also DAQmx_Val_PseudoDiff
	DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle, "Dev1/ai0", "", DAQmx_Val_Diff, -0.1, 0.1, DAQmx_Val_Volts, NULL));
	//DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle, "Dev1/ai0", "", DAQmx_Val_Cfg_Default, -10.0, 10.0, DAQmx_Val_Volts, NULL));
    // the numbr 2 comes from 'pretrigger samples'; this number has to be >=2 (this is a HW constrain!)
	DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle, "", 1/hw_data->St_A, DAQmx_Val_Rising, DAQmx_Val_FiniteSamps, hw_data->Ns_A+2));
	DAQmxErrChk (DAQmxCfgDigEdgeRefTrig(taskHandle, "/Dev1/PFI9", DAQmx_Val_Rising, 2));

	/*********************************************/
	// DAQmx Start Code
	/*********************************************/
	DAQmxErrChk (DAQmxStartTask(taskHandle));

	/*********************************************/
	// DAQmx Read Code
	/*********************************************/
	ret_err = DAQmxReadAnalogF64(taskHandle, DAQmx_Val_Auto, 12.0, DAQmx_Val_GroupByChannel, hw_data->sigA, DBUF_LEN, &read, NULL);
    if(ret_err<0) printf("Probably time-out by data acquisition!\n");
	DAQmxErrChk (ret_err);

	printf("Acquired %d points. First sample %g\n", (int) read, hw_data->sigA[0]);

    if( taskHandle!=0 ){
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
	}



//##### END OF THE RUN INITIALISATION    


#ifdef TIMING_REPORTING
    update_ttrigger=1;  // to update this time only once! (is here because of cases where cycle_time>1s!!!)
    clock_gettime(CLOCK_REALTIME, &tstart);
#endif
        
    // NOW we start everything at the same time!
    ////DAQmxErrChk (DAQmxConnectTerms(HW_TRIGER_DISC_TERM, GLOBAL_START_PIN, DAQmx_Val_DoNotInvertPolarity)); 
    printf("HW_RUN: Waiting for trig and generating DAQ sequence...\n");

    //wait until done! the wait time seems to be in 's'
    ///for(dev_i=0;dev_i<MAX_DEV;dev_i++) 
    ///    for(j=1;j<eMaxHandle;j++)       // we start from AODynamic here!
    ///        DAQmxErrChk (DAQmxWaitUntilTaskDone(tasks[dev_i][j].handle, hw_data->cycle_time * 2));

#ifdef TIMING_REPORTING
    clock_gettime(CLOCK_REALTIME, &tend);
    double time_spent = (tend.tv_sec - tstart.tv_sec) + (tend.tv_nsec - tstart.tv_nsec) / 1e9;
    double time_generation = (tend.tv_sec - ttriger.tv_sec) + (tend.tv_nsec - ttriger.tv_nsec) / 1e9;
    if(alreadyRunning){
        printf("\n");
        alreadyRunning=0;
    }
    printf("HW_RUN: 'wait for trig' + 'generation' took %f s. 'generation' took %f s.\n------------------------\n", time_spent, time_generation);
    //update statistics:
    timing_n++;                                                                 //count
    if(time_spent < timing_statistics[0]) timing_statistics[0] = time_spent;    //min
    if(time_spent > timing_statistics[2]) timing_statistics[2] = time_spent;    //max
    timing_statistics[1] += time_spent;                                         //sum
#else
    printf("HW_RUN: generation finished.\n");
#endif

    return(0);


Error:
    if( DAQmxFailed(error) ){
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        printf("HW_RUN: ni-DAQmx Error: %s\n",errBuff);
        for(dev_i=0;dev_i<MAX_DEV;dev_i++) 
            for(j=1;j<eMaxHandle;j++)       // we start from AODynamic here!
                DAQmxStopTask(tasks[dev_i][j].handle);
        DAQmxStopTask(mcsHandle);
    }
    for(dev_i=0;dev_i<MAX_DEV;dev_i++)
        for(j=1;j<eMaxHandle;j++){         // we start from AODynamic here! AND all the tasks shall be stopped, since no error
            DAQmxClearTask(tasks[dev_i][j].handle);
            tasks[dev_i][j].handle=0;
        }
    DAQmxClearTask(mcsHandle);
    mcsHandle=0;
    
    if( DAQmxFailed(error) ) return(-1);
    return(0);
}


int CVICALLBACK hw_DoneCallback(TaskHandle taskHandle, int status, void *callbackData){
    int error=0;
    char errBuff[2048]={'\0'};

    // Check to see if an error stopped the task.
    DAQmxErrChk (status);

Error:
    if( DAQmxFailed(error) ){
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        DAQmxClearTask(taskHandle);
        printf("callback: DAQmx Error: %s\n", errBuff);
    }else{
        error=0;
        //printf("callback: DAQmx callback with no Error.\n");
    }
    return 0;
}


//initialise the data structure 
void hw_initialize_data_struct(struct cards_data * data){
    data->St_A = 0.001;
    data->Ns_A = 50;
}


//this function formats the data from hw_data structure to be then sent over TCP
void getMeasDataStr(char *msg_in){
    char *msg=msg_in;
    int i;
    //printf("STRinging now\n"); 

    msg += sprintf(msg, "D0:"); 
	// this is sigA + 2; has to be like this, because of at least 2 pretrigger samples!
    for(i=2;i<hw_data->Ns_A+2;i++) msg += sprintf(msg, "%g,", hw_data->sigA[i]);
    
    //*(msg - 1) = '\0';  //delete the last comma!
    msg += sprintf(msg, "\n"); 

    //printf("STR:%s", msg_in); 
}


#ifdef SELF_TEST_MAIN
int main(int argc, char **argv){
    volatile int i;
    char errBuff[2048]={'\0'};

    //only testing!!!!
    new_data = (struct cards_data *) malloc(sizeof(struct cards_data));
    hw_data = (struct cards_data *) malloc(sizeof(struct cards_data));

    //create some data for the run 'run'
    //THIS IS NOT WORKING! ONE HAS TO GENERATE DATA!!!!!
    memcpy(hw_data, new_data, sizeof(struct cards_data));

    
    hw_reset(errBuff);

    hw_init(errBuff);
    
#define NUM_RUNS 10
    for(i=0;i<NUM_RUNS;i++){
        printf("Run: %d/%d\n", i, NUM_RUNS);
        hw_run(errBuff);
    }

    sleep(1);
    hw_safeOutput(errBuff);


#ifdef TIMING_REPORTING
    printf("HW_RUN: timing statistics [s]: min= %f; max= %f; avg= %f;\n", timing_statistics[0], timing_statistics[2], timing_statistics[1]/timing_n);
#endif

    return 0;
}
#endif //SELF_TEST_MAIN


/* NOTES:

I have found this for arming a counter:
    //DAQmxErrChk (DAQmxSetArmStartTrigType(task->handle, DAQmx_Val_DigEdge));  //'DAQmx_Val_DigEdge' or 'DAQmx_Val_None'
    //DAQmxErrChk (DAQmxSetDigEdgeArmStartTrigSrc(task->handle, task->triggerStr));
    //DAQmxErrChk (DAQmxSetDigEdgeArmStartTrigEdge(task->handle, DAQmx_Val_Rising));


COUNTERS OUTPUTS: !!!!!  IMPORTANT!!!

ctr0 and ctr2 share PFI2 and ctr1 and ctr3 share PFI7 as output.
ni-daqmx is so stupid, that it conets this by default!!! Thus, 2 counters will not work at the same time (at least daqmx complains),
unles you set one pin somewhere else. And here comes the problem!

//DAQmxErrChk( DAQmxSetCOPulseTerm( mcsHandle, "Dev1/ctr2", "RTSI4"));  
//DAQmxErrChk (DAQmxExportSignal(mcsHandle, DAQmx_Val_CounterOutputEvent,"RTSI4"));
*/
