#!/usr/bin/python3

###############################################################################
###############################################################################
#
# This tool plots what it gets on the stdin as s super simple x,y plot
# it is very usefull for something like this:
#     cat t.in | ./stdio_show.py -d 1
#                 or
#     ./hw-server | ./stdio_show.py
#     (for a program that generates data on stdout periodically)
# or any other pie from a program that provides meaningful string output
#
###############################################################################
###############################################################################

import logging, logging.handlers, socket, subprocess, sys, time
import numpy as np
from scipy.signal import get_window
from datetime import datetime
from time import sleep

import matplotlib
matplotlib.use('TkAgg')
#matplotlib.rcParams['toolbar'] = 'None'    #uncoment to hide toolbar
from matplotlib import pyplot as plt
#from collections import deque


VERBOSE=False


################################################################################
# Events
def onresize(event):
    plt.tight_layout()

def on_key_press(event):
    print('press', event.key)
    print('len', len(event.key))
    sys.stdout.flush()
    if event.key in 'qQeE':
        sys.exit(-1)
    #if event.key == 'p':            #pauses the DAQ cycle (sleep 1s and then check again)
    #    PAUSED = True if PAUSED == False else False
    #    fig.canvas.draw()


########### ENTRY POINT ###########
if __name__ == '__main__':
    import argparse
    import fileinput
    parser = argparse.ArgumentParser(description='Plot data provided on the stdin line.', \
    epilog='NOTE: Data has to look like this: PLT:[trace_nr]:[X_step]:0,0,0,0,0,:END\
            X_step is the step between the points on X axis, the data afterwards are then\
            Y - points.\
            trace_nr - supports up to 3 traces (0 - red, 1 - blue, 2 - magenta).')

    parser.add_argument('-l', '--log', action='store_true', help='Y axis in log scale')
    parser.add_argument('-d', '--delay', type=float, default=0.0, help='delay between proceeding to the next line in input')
    parser.add_argument('files', metavar='FILE', nargs='*', help='files to read, if empty, stdin is used')
    
    args = parser.parse_args()

    #if args.verbose == True:
    #    VERBOSE = True



    fig, axarr = plt.subplots(2, 1)
    ax, bx = axarr
    fig.canvas.mpl_connect('resize_event', onresize)
    #fig.canvas.mpl_connect('close_event', handle_close)
    fig.canvas.mpl_connect('key_press_event', on_key_press)

    ax.set_title("Signal:    (iter:{:d})".format(0), loc='left')
    #ax.set_xlabel('t')
    #ax.set_ylabel('I')
    if args.log: ax.set_yscale('log')


    plt.show(block=False)
    plt.draw()

    ax.set_title("Signal CNTR:", loc='left')
    bx.set_title("Signal ADC:", loc='left')
    
    points = { i: ax.plot([], [], '-', color=color)[0] for i, color in enumerate(['r', 'b', 'm']) }
    points.update( {i+80: bx.plot([], [], '-', color=color)[0] for i, color in enumerate(['r', 'b', 'm', 'g', 'k', 'cyan', 'yellow', 'brown']) } )

    #ATTENTION: this has to be done like this, if using argparse and fileinput.inpu()
    #at the same time, otherwise it crashes here, if using cmd line params!
    iteration = 0
    for line in fileinput.input(files=args.files if len(args.files) > 0 else ('-', )):
        try:
            line.strip()
            s_st, s_trace_nr, s_X_step, s_data, s_end = line.split(':')
            if s_st != "PLT" or s_end.strip() != "END":
                a = 1.0/0.0
            if s_trace_nr not in ['0', '1', '2', '80', '81', '82', '83', '84', '85', '86', '87']: 
                s_trace_nr = '0'
            trace_nr = int(s_trace_nr)
            if s_X_step == '': x_step = 1
            else:
                x_step = float(s_X_step)
        except:
            print("N/A:{}\n".format(line.strip()))
            continue
        
        p_y = []
        for i in s_data.split(','):
            try:
                p_y.append(float(i))
            except ValueError:
                continue
        
        print("Ploting trace {}; x_step = {}; points = {}".format(s_trace_nr, x_step, len(p_y)))
        p_y = np.array(p_y)
        p_x = np.arange(0, len(p_y)*x_step, x_step)
        points[trace_nr].set_data(p_x, p_y)
        iteration += 1
        for ax in axarr:
            ax.relim()
            ax.autoscale_view()
        plt.tight_layout()
        fig.canvas.draw()
        fig.canvas.flush_events()
        #print(p_x, p_y)

        sleep(args.delay) 


    print("Ended!")
    #time.sleep(10)

