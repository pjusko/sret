#include <stdio.h> 
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <pthread.h> 
#include <errno.h> 
#include <NIDAQmx.h>

/*****
ATTENTION! this is an attempt to make this multithreadid by design! ALL THE nidaqmx tasks have their own thread anyway!
(this is not obvious, but it is like this...)

The programm shall be run like this:
./hw-server | ./stdio_show.py

1. thread - main (is left in while(1); sleep(1) forever, after configuration)
2. thread - blinkny (does on/off LED)
   this is a real thread (ours); other directives can be put in between the on/off of the led.
3. counter1 is a 'sample clock generator' for counter0
4. counter0 is a edge counter, sampled by the sample_clok on RTSI6 (provided by ctr1)
5. DI on port0, that uses the RTSI6 as sample clock
   -- these 3 channels are plotted in the top panel of stdio_show.py
6. DAC uses RTSI6 as sample clock and outputs some triangle wave continuously
7. ADC is ON all the time and also uses RTSI6 as sample clock. The data is transfered back the same way the 
   counter is and can be plotted. Is done in a multichannel mode already! Just change ADC_STRING and ADC_CHNNELS
   accordingly!
   -- the ADC channels (up to 8) are plotted in the bottom panel of the stio_show.py


Ctr0 and DI have their own callbacks, that print their respective data, as soon as it is acquired
(THIS is the main difference to the other MultiChannelScaler program)

DI -- carefull with all the 'lines'. The way it is programed here, is now working as expected, i.e.,
    the U32 contains all the bits, as defined in the 'configuration string 'port0/lines0:1'.
    I have not been able to make it work with U8; I do not know why... maybe my missconfiguration..


UNDOCUMENTED functions are (see NIDAQmx.h):
DAQmxSetCOPulseTerm      -- for setting the output pin of the counter
DAQmxSetCICountEdgesTerm -- for setting the pin on which the pulse train is sampled

OPEN PROBLEMS:
The counter 0 does not reset the value!!! I do not know, how to do this 
automatically after every 'n' points...    (thought there has to be a function etc..)
THEREFOR, the workaround now is 'removeAccumulation' which simply returns the 
difference between each 2 ctr0 'snapshots'.

Here I learned, ni-daqmx is multithreaded by design (every task is one thread):
https://forums.ni.com/t5/Measurement-Studio-for-VC/DAQmx-how-does-it-multithread/td-p/221953
*****/




/*********************************************************************
* originally this was inspired by:
*
* ANSI C Example program:
*    ContAcq-IntClk.c
*
* Example Category:
*    AI
*
* but now it has been modified so much, that it doesn't even matter.
*********************************************************************/
static int iterationADC, iterationDIs;

#define DATA_LEN_ADC 1000
#define ADC_STRING   "Dev1/ai0,Dev1/ai1,Dev1/ai2"
#define ADC_CHANNELS 3

#define DATA_LEN 1000
static uInt32 data[DATA_LEN];

#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else


int32 CVICALLBACK EveryNSamplesCallbackCtr0(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
int32 CVICALLBACK EveryNSamplesCallbackDI(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);
int32 CVICALLBACK EveryNCallbackADC(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData);

int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData);


int out_print(int trace, float sampling_period, uInt32 *data, int len, int removeAccumulation){
    // print data in the format for stdio_show.py
    int i;
    uInt32 last=0;

    printf("PLT:%d:%f:", trace, sampling_period);
    for(i=0; i<len; i++){
        if(removeAccumulation){
            if(i!=0) printf("%d,", data[i]-last);
            last = data[i];
        }else{
            printf("%d,", data[i]);
        }
    }
    printf(":END\n");
    return(0);
}


int out_print_float(int trace, float sampling_period, float64 *data, int len, int channels){
    // print data in the format for stdio_show.py
    int i, ch;

    for(ch=0;ch<channels; ch++){
        printf("PLT:%d:%f:", trace+ch, sampling_period);
        for(i=0; i<len; i++) printf("%f,", data[i+ch*len]);
        printf(":END\n");
    }
    return(0);
}


void *blinky_thread(void * params){
	int32       error=0;
	TaskHandle  taskHandle=0;
	char        errBuff[2048]={'\0'};
    int do_print = 0;

    uInt8       data[1]={0};

    if(((char *) params)[0] != '0') do_print=1;

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
	DAQmxErrChk (DAQmxCreateTask("Blinky", &taskHandle));
    DAQmxErrChk (DAQmxCreateDOChan(taskHandle,"Dev1/port2/line1","",DAQmx_Val_ChanForAllLines));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));


    while(1){
        /*********************************************/
        // DAQmx Write Code
        /*********************************************/
        DAQmxErrChk (DAQmxWriteDigitalLines(taskHandle, 1 , 1, 10.0, DAQmx_Val_GroupByChannel, data, NULL, NULL));
        if(data[0] == 0) data[0] = 1;
        else data[0] = 0;
        if(do_print) printf("\nBlinky t: Setting the LED to : %d\n", data[0]);
        sleep(1);
    }

Error:
	if( DAQmxFailed(error) )
		DAQmxGetExtendedErrorInfo(errBuff,2048);
	if( taskHandle!=0 ) {
		/*********************************************/
		// DAQmx Stop Code
		/*********************************************/
		DAQmxStopTask(taskHandle);
		DAQmxClearTask(taskHandle);
	}
	if( DAQmxFailed(error) )
		printf("Blinky t: DAQmx Error in blinky_thread : %s\n",errBuff);
    return(NULL);
}


int run_sample_clock_counter(float sampling_period){
    int         error=0;
    TaskHandle  taskHandle=0;
    char        errBuff[2048]={'\0'};

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
    DAQmxErrChk (DAQmxCreateCOPulseChanFreq(taskHandle,"Dev1/ctr1","",DAQmx_Val_Hz,DAQmx_Val_Low,0.0,1.00/sampling_period,0.50));
    DAQmxErrChk (DAQmxCfgImplicitTiming(taskHandle,DAQmx_Val_ContSamps,1000));

    DAQmxErrChk (DAQmxRegisterDoneEvent(taskHandle,0,DoneCallback,NULL));

    DAQmxErrChk (DAQmxSetCOPulseTerm(taskHandle, "Dev1/ctr1", "PFI13"));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));

    printf("Generating pulse train as sample clock on counter1...\n");
    return(0);

Error:
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("End of program, press Enter key to quit\n");
    getchar();
    return 0;
}





/* This is just contant output on DAC0 
 * output is triangular wave
 */
int run_triangle_dac(int sampleLen, double Amplitude1, double Amplitude2){
    int         i,error=0;
    TaskHandle  taskHandle=0;
    char        errBuff[2048]={'\0'};
    double      data[2048];

    //generate triangle waveform
    for(i=0;i<sampleLen;i++) data[i] = Amplitude1 * (2*i/(sampleLen+0.0001)-1);
    //2nd channel
    for(i=0;i<sampleLen;i++) data[i+sampleLen] = Amplitude2 * (2*i/(sampleLen+0.0001)-1);
    
    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    // ANALOG PULSED ---------------------------------------------------------------
    DAQmxErrChk (DAQmxCreateTask("DacOut", &taskHandle));
    DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle, "Dev1/ao0,Dev1/ao1", "", -10.0, 10.0, DAQmx_Val_Volts, NULL));
    DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle, "RTSI6", 1000000.0, DAQmx_Val_Rising, DAQmx_Val_ContSamps, sampleLen)); //DAQmx_Val_ContSamps or DAQmx_Val_FiniteSamps
    //the next line shows how to set the trigger; we just use 'SW trigger' so nothing has to be done
    //DAQmxErrChk (DAQmxCfgDigEdgeStartTrig(taskHandleD, "/Dev1/RTSI2", DAQmx_Val_Rising));

    DAQmxErrChk (DAQmxRegisterDoneEvent(taskHandle, 0, DoneCallback, NULL));
    DAQmxErrChk (DAQmxWriteAnalogF64(taskHandle, sampleLen, 0, 1, DAQmx_Val_GroupByChannel, data, NULL, NULL));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));

    printf("Generating triangle waveform on the DACs...\n");
    return(0);

Error:
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("run_triangle_dac: End of program, press Enter key to quit\n");
    getchar();
    return 0;
}


/* This is just constant ADC in calling EveryNCallbackADC when buff is full 
 * can be used for multiple channels at once! see ADC_STRING
 */
int run_adc(void){
    int         error=0;
    TaskHandle  taskHandle=0;
    char        errBuff[2048]={'\0'};

    /*********************************************/
    // DAQmx Configure Code
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("ADC IN",&taskHandle));
    DAQmxErrChk (DAQmxCreateAIVoltageChan(taskHandle, ADC_STRING,"",DAQmx_Val_Diff,-10.0,10.0,DAQmx_Val_Volts,NULL));
    DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle,"RTSI6",100000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000));
    //we use software start!!!  DAQmxErrChk (DAQmxCfgDigEdgeStartTrig(taskHandle,"/Dev1/PFI0",DAQmx_Val_Rising));
    // possible channel configurations: DAQmx_Val_Cfg_Default
    //DAQmx_Val_RSE           Referenced single-ended mode
    //DAQmx_Val_NRSE          Non-referenced single-ended mode
    //DAQmx_Val_Diff          Differential mode
    //DAQmx_Val_PseudoDiff    Pseudodifferential mode

    DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(taskHandle,DAQmx_Val_Acquired_Into_Buffer,DATA_LEN_ADC,0,EveryNCallbackADC,NULL));
    DAQmxErrChk (DAQmxRegisterDoneEvent(taskHandle,0,DoneCallback,NULL));

    /*********************************************/
    // DAQmx Start Code
    /*********************************************/
    DAQmxErrChk (DAQmxStartTask(taskHandle));

    printf("Started continuous ADC acq...\n");
    return(0);

Error:
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandle!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
    }
    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("run_adc: End of program, press Enter key to quit\n");
    getchar();
    return 0;

}



int main(int argc, char **argv){
	int32       error=0;
	TaskHandle  taskHandleC=0, taskHandleD=0;
	char        errBuff[2048]={'\0'};

    float       sampling_period=1e-3;   //this sahll be in seconds 1kHz

    pthread_t thread_blinky;
    char *params="0 - Bla bla bla"; // If the first character is '0' blinky thread will not print anything! Otherwise it will

    pthread_create(&thread_blinky, NULL, blinky_thread, (void *) params );




/////////////////////////////////  Initialize counter1 as counter for "sampling period/frequency"

    run_sample_clock_counter(sampling_period);

    DAQmxErrChk (DAQmxConnectTerms("/Dev1/Ctr1InternalOutput", "/Dev1/RTSI6", DAQmx_Val_DoNotInvertPolarity));


/////////////////////////////////  Initialize and run the DAC0 triangular output
    run_triangle_dac(100, 5, -2.5);

/////////////////////////////////  Initialize and run the ADC function (calls callback!)
    run_adc();

/////////////////////////////////  Initialize and read counter0 as 'event-counter-buffered' (i.e. our MCS)


    /*********************************************/
    // DAQmx Configure Code COUNTER
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("",&taskHandleC));
    DAQmxErrChk (DAQmxCreateCICountEdgesChan(taskHandleC,"Dev1/ctr0","",DAQmx_Val_Rising,0,DAQmx_Val_CountUp));
    DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandleC,"/Dev1/RTSI6",1000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000));

    DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(taskHandleC,DAQmx_Val_Acquired_Into_Buffer,1000,0,EveryNSamplesCallbackCtr0,NULL));

    DAQmxErrChk (DAQmxSetCICountEdgesTerm(taskHandleC, "/Dev1/Ctr0", "/Dev1/PFI8"));

    // DAQmx Start Code 
    DAQmxErrChk (DAQmxStartTask(taskHandleC));

    //------------------------------------------------------------------------------------------------------------------------------

    /*********************************************/
    // DAQmx Configure Code DIGITAL IN
    /*********************************************/
    DAQmxErrChk (DAQmxCreateTask("",&taskHandleD));
	DAQmxErrChk (DAQmxCreateDIChan(taskHandleD,"Dev1/port0/line0:1","",DAQmx_Val_ChanForAllLines));//DAQmx_Val_ChanPerLine));
    /* Note on how the cahnnels are organised!
     * apparently one gets at least 1 byte per 'lineX' although DAQmxReadDigitalU8 shall work differently!
     */
	DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandleD,"/Dev1/RTSI6",10000.0,DAQmx_Val_Rising,DAQmx_Val_ContSamps,1000));
    DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(taskHandleD,DAQmx_Val_Acquired_Into_Buffer,1000,0,EveryNSamplesCallbackDI,NULL));

    // DAQmx Start Code 
    DAQmxErrChk (DAQmxStartTask(taskHandleD));




    printf("Continuously reading. Press Ctrl+C to interrupt\n");

    while( 1 ) {
        /*********************************************/
        // DAQmx Read Code
        /*********************************************/

        /*int32       read;     //this was the old sequential code
         *DAQmxErrChk (DAQmxReadCounterU32(taskHandleC,1000,10.0,data,1000,&read,NULL));
         *out_print(0, sampling_period, data, DATA_LEN, 1);
         */

        sleep(1);
        //printf("\rAcquired %d samples",(int)read);
        //fflush(stdout);
    }

Error:
    puts("");
    if( DAQmxFailed(error) )
        DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( taskHandleC!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandleC);
        DAQmxClearTask(taskHandleC);
    }
    if( taskHandleD!=0 ) {
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandleD);
        DAQmxClearTask(taskHandleD);
    }

    if( DAQmxFailed(error) )
        printf("DAQmx Error: %s\n",errBuff);
    printf("End of program, press Enter key to quit\n");
    getchar();
    return 0;
}




/*************************************************************************************/
//                                    CALLBACKS   
/*************************************************************************************/

int32 CVICALLBACK DoneCallback(TaskHandle taskHandle, int32 status, void *callbackData)
{
    int32   error=0;
    char    errBuff[2048]={'\0'};
    char    name[100]={'\0'};
    
    DAQmxErrChk(DAQmxGetTaskName(taskHandle, name, 100))

    // Check to see if an error stopped the task.
    printf("DoneCallback: task: %s\n", name);
    DAQmxErrChk (status);

Error:
    if( DAQmxFailed(error) ) {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        DAQmxClearTask(taskHandle);
        printf("DoneCallback: DAQmx Error: %s\n",errBuff);
    }
    return 0;
}


int32 CVICALLBACK EveryNSamplesCallbackCtr0(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData){
	int32   error=0;
    char    errBuff[2048]={'\0'};

    int32       read;
    float       sampling_period=1e-3;   //this sahll be in seconds

    printf("EveryNSampleCallbackCtr0. Samples: %d\n", nSamples);
    
    DAQmxErrChk (DAQmxReadCounterU32(taskHandle,1000,10.0, data,1000,&read,NULL));
        
    out_print(0, sampling_period, data, DATA_LEN, 1);

Error:
    if( DAQmxFailed(error) ) {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        DAQmxClearTask(taskHandle);
        printf("DAQmx Error: %s\n",errBuff);
    }
    return 0;
}


int32 CVICALLBACK EveryNSamplesCallbackDI(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData){
	int32   error=0;
    char    errBuff[2048]={'\0'};
    uInt32  dataD[2*DATA_LEN];

    int32       read;
    float       sampling_period=1e-3;   //this sahll be in seconds

    printf("EveryNSampleCallbackDI. Iter: %d Samples: %d\n", iterationDIs++, nSamples);
    
    DAQmxErrChk (DAQmxReadDigitalU32(taskHandle,1000,10.0, DAQmx_Val_GroupByChannel, dataD,1000,&read,NULL));
        
    out_print(1, sampling_period, dataD, DATA_LEN, 0);

Error:
    if( DAQmxFailed(error) ) {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        DAQmxClearTask(taskHandle);
        printf("DAQmx Error: %s\n",errBuff);
    }
    return 0;
}


int32 CVICALLBACK EveryNCallbackADC(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
    int32       error=0;
    char        errBuff[2048]={'\0'};
    //static int  totalRead=0;
    int32       read=0;
    float64     data[9000];

    printf("EveryNCallbackADC. Iter: %d Samples: %d\n", iterationADC++, nSamples);

    DAQmxErrChk (DAQmxReadAnalogF64(taskHandle,DATA_LEN_ADC,10.0,DAQmx_Val_GroupByChannel, data, 9000, &read, NULL));

    //int out_print_float(int trace, float sampling_period, float64 *data, int len, int channels){
    out_print_float(80, 1e-3, data, DATA_LEN_ADC, ADC_CHANNELS);


Error:
    if( DAQmxFailed(error) ) {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
        /*********************************************/
        // DAQmx Stop Code
        /*********************************************/
        DAQmxStopTask(taskHandle);
        DAQmxClearTask(taskHandle);
        printf("DAQmx Error: %s\n",errBuff);
    }
    return 0;
}

