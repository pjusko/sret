#!/usr/bin/python3

import logging, logging.handlers, socket, subprocess, sys, time
import numpy as np
from scipy.signal import get_window
from datetime import datetime

import matplotlib
matplotlib.use('TkAgg')
#matplotlib.rcParams['toolbar'] = 'None'    #uncoment to hide toolbar
from matplotlib import pyplot as plt
#from collections import deque

OUT_DIR = "OUT/"

PAUSED=False
VERBOSE=False
s = None


################################################################################
# Events
def onresize(event):
    plt.tight_layout()

def handle_close(event):
    global s
    print('Closing Socket!')
    s.close()
    sys.exit()

def on_key_press(event):
    global PAUSED
    #print('press', event.key)
    #sys.stdout.flush()
    if event.key == 'p':            #pauses the DAQ cycle (sleep 1s and then check again)
        PAUSED = True if PAUSED == False else False
        fig.canvas.draw()
    if event.key == 'q':            #quit
        fig.canvas.draw()
        handle_close(event)

################################################################################


def is_ip_addr(addr):
    """ checks if addr is an ip v4 address """
    if len(addr.split('.')) != 4: return(False)
    for x in addr.split('.'):
        try:
            i = int(x)
        except ValueError:
            return(False)
        if not (i <= 255 and i>=0): return(False)

    return(True)


def recv_line(s):
    """ recieve tcp data until \n found """ 
    data = bytearray()
    error = None
    while True:
        try: 
            data += s.recv(4096)
            #print('Recieve returned:', repr(data[0:-1]))
        except OSError as err:
            print("we got to TCP error!")
            error = err
            return(False)
        if data.find(b'\n')>=0 : 
            if VERBOSE:
                print("Got DATA:{}".format(data.decode('ascii')))
            return(data.decode('ascii'))

def store_data(x, y):
    """ Creates a file with current date time name and stores the 
        x, y SPACE separated data
    """
    now = datetime.now()
    msec = now.strftime("%f")
    fname = now.strftime("%Y_%m_%d-%H%M%S") + "-{:.1}".format(msec) + ".dat"
    print("fname:", fname)

    f = open(OUT_DIR + fname, "w")
    f.write("#data dump file: {}\n".format(fname))
    for i in range(len(x)):
        f.write("{:.5f} {:.5f}\n".format(x[i], y[i]))
    f.close()

def moving_average(a, w):
    """ caluclates a simple mooving average using convolute """
    return(np.convolve(a, np.ones(w), 'valid')/w)

def show_tcp_data(fig, ax, HOST, PORT, t_bin=0.01, n_bins=10, ylog=False, bx=None, n=1000, window='boxcar', dump=False, avg_len=200):
    global s, PAUSED
    runs=0
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        try:
            s.connect((HOST, PORT))
        except OSError as err:
            return(err)

        # get the welcome message
        line = recv_line(s)
        print("Recieved line:", line)
        line = line.strip()
        if line.find("Hello! I am SRET CMD.")<0:
            return("This is not SRET CMD server...")

        #parse the number of BINS:
        s.sendall('L -1 0;C 1.0;S St_A={:.5f};S Ns_A={:d};\n'.format(t_bin, n_bins).encode('ascii'))
        
        for i in range(0, 2):   #we shall got Ok. twice, since we send 2 s commands!!!
            line = recv_line(s)
            if line.find('Ok.')<0:
                return("Wrong response to Initialisation string!")

        xdataM = np.arange(0, n_bins*t_bin, t_bin) 
        ax.set_xlabel('t (s)')
        ax.set_ylabel('')
        #text = plt.title("MCS", loc='left')

        ax.set_title("Signal:    (bins:{:d})    RUN:{:d}".format(n_bins, runs), loc='left')
        if bx!=None:
            bx.set_xlabel('f (Hz)')
            bx.set_ylabel('')
            if ylog: bx.set_yscale('log')
            #returns the frequency axis from time axis
            fft_x = np.fft.fftfreq(n_bins, t_bin)[:n_bins//2]
            #boxcar window means, there is no window!
            try:
                fft_window = get_window(window, n_bins)
            except:
                return("Probably wrong name of the fft window!")
            #bx.text(0.8, 0.9, "win={}".format(window), fontsize=8, transform = bx.transAxes)
            bx.set_title("FFT:  (window={})  Paused:{} ('p' to cycle) AVG: {:.3g}".format(window, PAUSED, -1.0), loc='left')

        plt.show(block=False)
        plt.draw()

        pointsM = ax.plot([], [], '-', color='r')[0]
        if avg_len>0:
            pointsAVG = ax.plot([], [], '-', color='k')[0]
            pointsAVGx = np.array([])
        
        if bx!=None:
            pointsFFT = bx.plot([], [], '-', color='r')[0]

        #textS = fig.text(0.8, 0.95, '', va='top', ha='center', bbox=dict(facecolor='white', edgecolor='none', pad=.0))
        sig_m, sig_s, fft_m = 0, 0, 0
        while True:
            fig.canvas.draw()
            fig.canvas.flush_events()
            if bx!=None:
                bx.set_title("FFT:  (window={})  Paused:{} ('p' to cycle) AVG: {:.3g}".format(window, PAUSED, fft_m), loc='left')
            if PAUSED:
                time.sleep(0.1)
                continue
            s.sendall(b'R 1\n') #update time: ,0 - 100ms; ,1 - 1s
            s.settimeout(10.0) #I set timeout for 10s, since there should always be something 'earlier'
            line = recv_line(s)
            if line.find('Ok.')<0:
                return("Wrong response to 'R 1' command!")

            data = recv_line(s)

            if data != False:
                #here we should check, if meaningful data has been recieved+log it
                if data[0] == 'D':
                    runs+=1
                    if VERBOSE:
                        print("Run: {:d} - got data".format(runs))
                    ydataM = np.fromstring(data.split(':')[1], dtype=float, sep=',')[0:len(xdataM)]
                    sig_m, sig_s = ydataM.mean(), ydataM.std()
                    #print("Len of data is X:{} Y:{}".format(len(xdataM), len(ydataM)))
                    pointsM.set_data(xdataM, ydataM)
                    #alternatively store the data:
                    if dump:
                        store_data(xdataM, ydataM)
                    #alternatively plot the average:
                    if avg_len>0:
                        if len(pointsAVGx)<1:
                            a_window = int(len(xdataM)/avg_len)
                            pointsAVGx = moving_average(xdataM, a_window)
                        pointsAVGy = moving_average(ydataM, a_window)
                        pointsAVG.set_data(pointsAVGx, pointsAVGy)
                    ax.set_title("Signal:  (bins:{:d})  RUN:{:d} mean: {:.3g} std: {:.3g}".format(n_bins, runs, sig_m, sig_s), loc='left')
                    ax.relim()
                    ax.autoscale_view()
                    if bx!=None:
                        #original way
                        #fft_y = 2.0/n_bins * np.abs(np.fft.fft(ydataM)[0:n_bins//2])
                        #rfft shall be twice as fast since it only calculates half (i.e. 'real' component)
                        fft_y = np.abs(np.fft.rfft(ydataM*fft_window))[:-1]
                        fft_m = fft_y.mean()
                        pointsFFT.set_data(fft_x, fft_y)
                        bx.relim()
                        bx.autoscale_view()
                        if runs == 1:
                            print("First two frequencies FFT are {}    {}".format(fft_x[0], fft_x[1]))
                    plt.tight_layout()
                else:
                    print("Wiered DATA! :{}".format(data))
                    continue
            else:
                return("Recieve data error.")
    
    return("log_tcp_data ENDED (unexpected!)")


########### ENTRY POINT ###########
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Show MCS output on the screen.', \
    epilog='NOTE: All the parameters of the MCS have to be set in the server\
            that is running in the windows guest. Number of bins can be set as argv[1];\
            everything else in the file "pms300.ini".\n \
            NOTE: If You want to wait longer than 10 s for the incoming trigger;\
            You have to change the s.settimeout(10.0).')

    parser.add_argument('-a', '--address', type=str, help='host name', default='127.0.0.1')
    parser.add_argument('-p', '--port', type=int, help='host port', default=10700)
    parser.add_argument('-l', '--log', action='store_true', help='Y axis in log scale')
    parser.add_argument('-q', '--quiet', action='store_true', help='suspend printing to stdout')
    parser.add_argument('-N', '--n_bins', type=int, default=1000, help='number of bins/ samples to acquire per run')
    parser.add_argument('-T', '--t_bin', type=float, default=0.001, help='sampling period in s')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print all the recieved commands')
    parser.add_argument('-w', '--window', type=str, help='window for FFT', default='boxcar')
    parser.add_argument('-D', '--dump', action='store_true', help='save all the measurements in OUT direcotry')
    parser.add_argument('--avg', type=int, help='number of points to plot as AVG of measured data', default=200)
    #window can be: boxcar -- 'no window!!! hamming, hann, blackman, blackmanharris, bohman, triang, taylor
    #see: https://docs.scipy.org/doc/scipy/reference/signal.windows.html
    
    args = parser.parse_args()

    if args.verbose == True:
        VERBOSE = True

    if is_ip_addr(args.address) == False:
        print("This does not seem to be an IP address: {}".format(args.address))
        sys.exit(2)


    fig, axes = plt.subplots(2, 1)
    ax, bx = axes
    fig.canvas.mpl_connect('resize_event', onresize)
    fig.canvas.mpl_connect('close_event', handle_close)
    fig.canvas.mpl_connect('key_press_event', on_key_press)

    #if args.quiet:
    #    f = open(os.devnull, 'w')
    #    sys.stdout = f

    ret_err = show_tcp_data(fig, ax, args.address, args.port, bx=bx, ylog=args.log, n_bins=args.n_bins, t_bin=args.t_bin, 
        window=args.window, dump=args.dump, avg_len=args.avg)
    print("show_tcp_data ended with the error:\n    {}".format(ret_err))
    #time.sleep(10)

