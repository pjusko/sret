#!/usr/bin/python3

import logging, logging.handlers, socket, subprocess, sys, time
import numpy as np
from scipy.signal import get_window
from datetime import datetime


def read_data(fname):
    f = open(fname, 'r')
    x, y = [], []
    for l in f.readlines():
        if l[0] == '#': continue
        xi, yi = l.split()
        x.append(float(xi))
        y.append(float(yi))
    f.close()
    x = np.array(x)
    y = np.array(y)
    return(x, y)


def store_data(fname, x, y, head=""):
    f = open(fname, "w")
    f.write("#AVG file: {}; head_text={};\n".format(fname, head))
    for i in range(len(x)):
        f.write("{:.5f} {:.5f}\n".format(x[i], y[i]))
    f.close()


def moving_average(a, w):
    """ caluclates a simple mooving average using convolute """
    return(np.convolve(a, np.ones(w), 'valid')/w)



########### ENTRY POINT ###########
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Show MCS output on the screen.', \
    epilog='NOTE: All the parameters of the MCS have to be set in the server\
            that is running in the windows guest. Number of bins can be set as argv[1];\
            everything else in the file "pms300.ini".\n \
            NOTE: If You want to wait longer than 10 s for the incoming trigger;\
            You have to change the s.settimeout(10.0).')

    parser.add_argument('--avg', type=int, help='number of points to use as window for moving average', default=5)
    parser.add_argument('-v', '--verbose', action='store_true', help='Print all the recieved commands')
    parser.add_argument('fname', type=str, help='filename to process', default='')
    #window can be: boxcar -- 'no window!!! hamming, hann, blackman, blackmanharris, bohman, triang, taylor
    #see: https://docs.scipy.org/doc/scipy/reference/signal.windows.html
    
    args = parser.parse_args()

    if args.fname.find(".dat")<0:
        print("I do not recognise this file")
        exit(-1)
        
    x, y = read_data(args.fname)

    if args.verbose:
        print("File {} contains {} data-points".format(args.fname, len(x)))

    xa = moving_average(x, args.avg)
    ya = moving_average(y, args.avg)

    ofname = args.fname.replace(".dat", "avg.dat")
    store_data(ofname, xa, ya, head="window={}".format(args.avg))


