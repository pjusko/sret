#!/usr/bin/python3

import logging, logging.handlers, socket, subprocess, sys, time
import numpy as np
from scipy.signal import get_window
from datetime import datetime

import matplotlib
matplotlib.use('TkAgg')
#matplotlib.rcParams['toolbar'] = 'None'    #uncoment to hide toolbar
from matplotlib import pyplot as plt
#from collections import deque

OUT_DIR = "OUT/"

PAUSED=False
VERBOSE=False


################################################################################
# Events
def onresize(event):
    plt.tight_layout()

def handle_close(event):
    print('Closing Socket!')
    sys.exit()

def on_key_press(event):
    global PAUSED
    #print('press', event.key)
    #sys.stdout.flush()
    if event.key == 'p':            #pauses the DAQ cycle (sleep 1s and then check again)
        PAUSED = True if PAUSED == False else False
        fig.canvas.draw()
    if event.key == 'q':            #quit
        fig.canvas.draw()
        handle_close(event)

################################################################################


def is_ip_addr(addr):
    """ checks if addr is an ip v4 address """
    if len(addr.split('.')) != 4: return(False)
    for x in addr.split('.'):
        try:
            i = int(x)
        except ValueError:
            return(False)
        if not (i <= 255 and i>=0): return(False)

    return(True)


def recv_line(s):
    """ recieve tcp data until \n found """ 
    data = bytearray()
    error = None
    while True:
        try: 
            data += s.recv(4096)
            #print('Recieve returned:', repr(data[0:-1]))
        except OSError as err:
            print("we got to TCP error!")
            error = err
            return(False)
        if data.find(b'\n')>=0 : 
            if VERBOSE:
                print("Got DATA:{}".format(data.decode('ascii')))
            return(data.decode('ascii'))

def store_data(x, y):
    """ Creates a file with current date time name and stores the 
        x, y SPACE separated data
    """
    now = datetime.now()
    msec = now.strftime("%f")
    fname = now.strftime("%Y_%m_%d-%H%M%S") + "-{:.1}".format(msec) + ".dat"
    print("fname:", fname, msec)

    f = open(OUT_DIR + fname, "w")
    f.write("#data dump file: {}\n".format(fname))
    for i in range(len(x)):
        f.write("{:.5f} {:.5f}\n".format(x[i], y[i]))
    f.close()


def moving_average(a, w):
    """ caluclates a simple mooving average using convolute """
    return(np.convolve(a, np.ones(w), 'valid')/w)


def show_dat_data(fig, ax, fname, ylog=False, bx=None, window='boxcar', avg_len=200):
    global PAUSED

    f = open(fname, 'r')
    x, y = [], []
    for l in f.readlines():
        if l[0] == '#': continue
        xi, yi = l.split()
        x.append(float(xi))
        y.append(float(yi))

    xdataM = np.array(x)
    ydataM = np.array(y)

    n_bins = len(xdataM)
    t_bin = xdataM[1] - xdataM[0]


    ax.set_xlabel('t (s)')
    ax.set_ylabel('')
    #text = plt.title("MCS", loc='left')

    ax.set_title("Signal:    (bins:{:d})".format(n_bins), loc='left')
    if bx!=None:
        bx.set_xlabel('f (Hz)')
        bx.set_ylabel('')
        if ylog: bx.set_yscale('log')
        #returns the frequency axis from time axis
        fft_x = np.fft.fftfreq(n_bins, t_bin)[:n_bins//2]
        #boxcar window means, there is no window!
        try:
            fft_window = get_window(window, n_bins)
        except:
            return("Probably wrong name of the fft window!")
        #bx.text(0.8, 0.9, "win={}".format(window), fontsize=8, transform = bx.transAxes)
        bx.set_title("FFT:  (window={})  Paused:{} ('p' to cycle) AVG: {:.3g}".format(window, PAUSED, -1.0), loc='left')

    plt.show(block=False)
    plt.draw()

    pointsM = ax.plot([], [], '-', color='r')[0]
    
    if bx!=None:
        pointsFFT = bx.plot([], [], '-', color='r')[0]

    if bx!=None:
        bx.set_title("FFT:  (window={})  Paused:{} ('p' to cycle)".format(window, PAUSED), loc='left')

    sig_m, sig_s = ydataM.mean(), ydataM.std()
    #print("Len of data is X:{} Y:{}".format(len(xdataM), len(ydataM)))
    pointsM.set_data(xdataM, ydataM)
    
    if avg_len>0:
        a_window = int(len(xdataM)/avg_len)
        pointsAVGx = moving_average(xdataM, a_window)
        pointsAVGy = moving_average(ydataM, a_window)
        pointsAVG = ax.plot(pointsAVGx, pointsAVGy, '-', color='k')


    ax.set_title("Signal:  (bins:{:d})  mean: {:.3g} std: {:.3g}".format(n_bins, sig_m, sig_s), loc='left')
    ax.relim()
    ax.autoscale_view()
    if bx!=None:
        #original way
        #fft_y = 2.0/n_bins * np.abs(np.fft.fft(ydataM)[0:n_bins//2])
        #rfft shall be twice as fast since it only calculates half (i.e. 'real' component)
        fft_y = np.abs(np.fft.rfft(ydataM*fft_window))[:-1]
        fft_m = fft_y.mean()
        pointsFFT.set_data(fft_x, fft_y)
        bx.relim()
        bx.autoscale_view()
    plt.tight_layout()

    while True:
        fig.canvas.draw()
        fig.canvas.flush_events()
        time.sleep(0.1)
    
    return("log_tcp_data ENDED (unexpected!)")


########### ENTRY POINT ###########
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Show MCS output on the screen.', \
    epilog='NOTE: All the parameters of the MCS have to be set in the server\
            that is running in the windows guest. Number of bins can be set as argv[1];\
            everything else in the file "pms300.ini".\n \
            NOTE: If You want to wait longer than 10 s for the incoming trigger;\
            You have to change the s.settimeout(10.0).')

    parser.add_argument('-a', '--address', type=str, help='host name', default='127.0.0.1')
    parser.add_argument('-p', '--port', type=int, help='host port', default=10700)
    parser.add_argument('-l', '--log', action='store_true', help='Y axis in log scale')
    parser.add_argument('-q', '--quiet', action='store_true', help='suspend printing to stdout')
    parser.add_argument('-N', '--n_bins', type=int, default=1000, help='number of bins/ samples to acquire per run')
    parser.add_argument('-T', '--t_bin', type=float, default=0.001, help='sampling period in s')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print all the recieved commands')
    parser.add_argument('-w', '--window', type=str, help='window for FFT', default='boxcar')
    parser.add_argument('--avg', type=int, help='number of points to plot as AVG of measured data', default=200)
    parser.add_argument('fname', type=str, help='filename to process', default='')
    #window can be: boxcar -- 'no window!!! hamming, hann, blackman, blackmanharris, bohman, triang, taylor
    #see: https://docs.scipy.org/doc/scipy/reference/signal.windows.html
    
    args = parser.parse_args()

    if args.verbose == True:
        VERBOSE = True

    if is_ip_addr(args.address) == False:
        print("This does not seem to be an IP address: {}".format(args.address))
        sys.exit(2)


    fig, axes = plt.subplots(2, 1)
    ax, bx = axes
    fig.canvas.mpl_connect('resize_event', onresize)
    fig.canvas.mpl_connect('close_event', handle_close)
    fig.canvas.mpl_connect('key_press_event', on_key_press)

    #if args.quiet:
    #    f = open(os.devnull, 'w')
    #    sys.stdout = f

    ret_err = show_dat_data(fig, ax, args.fname, bx=bx, ylog=args.log, window=args.window, avg_len=args.avg)

def show_dat_data(fig, ax, fname, ylog=False, bx=None, window='boxcar'):
    print("show_tcp_data ended with the error:\n    {}".format(ret_err))
    #time.sleep(10)

