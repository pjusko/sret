#!/usr/bin/python3
import socket
import sys, time, random, signal
import numpy as np
from PyQt5.QtCore import QThread, QTimer, pyqtSignal, QEventLoop, pyqtSlot, QCoreApplication, QLocale
from noises import dc_error, white_noise, powerlaw_psd_gaussian

# Be very carefull witht his program. It uses 3 threads TIMer DAQ and CNT.
# It uses qt signal to start the timer from DAQ; but since CNT is always locked in the 'tcp recieve'
# the TIM thread calls the send_cnt() function directly! i.e. it is called from TIM thread! not form its own CNT thread.


TCP_IP = '127.0.0.1'
TCP_PORT = 10700
BUFFER_SIZE = 1024

#default buffer size and sampling interval!!!
DA_L = 500
SA_P = 0.001
tX = np.arange(0, DA_L*SA_P, SA_P)

DC_ERROR_PARAMS = [0, 1]    # ampl, exponent for DC_error
NOISES_PARAMS = [0, 0]       # ampl white noise, ampl pink noise
DC_ERROR = np.zeros_like(tX)


VERBOSE=False
RepRate = 0
IterationCounter = 0



def absmaxND(a, axis=None):
    amax = a.max(axis)
    amin = a.min(axis)
    return np.where(-amin > amax, amin, amax)


def generate_particle_data(verbose=False):
    """ Returns 'simulated' data for the light intenisty
        so far fixed tX is used for X axis!!! 
    """
    global IterationCounter, RandPhase, NOISES_PARAMS, tX
    y = np.zeros_like(tX)

    
    for particle_f in ReqParticles:
        phase = np.random.random()*2*np.pi if RandPhase else 0.0
        y += np.sin(2*np.pi*tX*particle_f + phase)

    #normalise:
    ymax = absmaxND(y)
    y = y/ymax

    #add noise:
    if ReqNoiseADD != 0:
        y = y + (np.random.random(len(y))-0.5)/100*ReqNoiseADD
    if ReqNoiseMUL != 0:
        y = y * (1 + np.random.random(len(y))/100*ReqNoiseMUL)

    #add dc_error
    y += DC_ERROR

    #add white noise
    if NOISES_PARAMS[0]>0:
        y += white_noise(len(tX), NOISES_PARAMS[0])

    #add pink noise
    if NOISES_PARAMS[1]>0:
        y += powerlaw_psd_gaussian(len(tX), exponent=1, fmin=0, ampl=NOISES_PARAMS[1])

    if verbose:
        #print all out:
        for i in range(len(tX)):
            print("{:8.4f} {:8.4f}".format(tX[i], y[i]))

    #transform to string:
    s_out = ''
    for i in y:
        s_out+="{:.4f},".format(i)

    IterationCounter += 1
    print("Generated data Iteration: {}".format(IterationCounter))
    return(s_out)

def defined_sleep():
    #print(time.time()) -- gets a very granular time; 

    time.sleep(1/RepRate)    #for now we just sleep 1/RepRate; RepRate is in Hz!!!


class DAQ_thread_Class(QThread):
    """
    Class emulating DAQ server running on Linux and comunnicating
    with the Concurent PCIe CARD

    THIS IS COMPLETELY SEQUENTIAL; COULD HAVE BEEN IMPLEMENTED WITHOUT A THREAD!!!
    """
    conn_close_signal = pyqtSignal()
    worker_signal = pyqtSignal()
    def __init__(self):
        QThread.__init__(self)

    @pyqtSlot()
    def worker(self):
        global tX, DC_ERROR, DA_L, SA_P
        while 1:
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)     #THIS HAS to be here, otherwise
            #the program will not start shortly after killing the previous one!!!
    
            self.s.bind((TCP_IP, TCP_PORT))
            print('hw-emulator.py: DAQ Listening...')
            self.s.listen(1)
            
            self.conn, self.addr = self.s.accept()
            print('hw-emulator.py: DAQ Connection address:', self.addr)

            self.conn.send("Hello! I am SRET CMD. Ver=0.1\r\n".encode('ascii'))
            
            #connection loop
            while 1:
                data=''
                break_out = False
                while True:
                    try:
                        data_p = self.conn.recv(BUFFER_SIZE)
                    except:
                        print("DAQ: exception in recieve data!")
                        break_out = True
                        break
                    if not data_p: 
                        break_out = True
                        break
                    if VERBOSE: print("\tDAQ: received data:", data_p)
                    try:
                        data += data_p.decode('ascii')
                    except:
                        data = ''
                    if data[-1] == '\n': break
                if break_out == True:
                    break

                cmd_list = data.split(';') # allow chaining of commands using ; as cmd delimiter!
                for cmd in cmd_list:
                    cmd = cmd.strip()
                    if len(cmd) == 0: continue
                    CMD, *params = cmd.split(' ')
                    if CMD == 'Q':
                        self.conn.send('Ok. State is ok -- dummy-hw.\r\n'.encode('ascii'))
                        continue
                    if CMD == 'R':
                        if len(params)==1:
                            try:
                                runs = int(params[0])
                            except ValueError:          #except IndexError:
                                self.conn.send('Ee: not a valid RUNS value.\r\n'.encode('ascii'))
                                continue
                        else:
                            self.conn.send('Ee: not a valid RUNS value.\r\n'.encode('ascii'))
                            continue

                        self.conn.send('Ok. Starting RUN.\r\n'.encode('ascii'))
                        defined_sleep() # <-- this thing is 'sleep' that waits for the 'full second' of the TIME in order to simulate real HW
                        data_str = generate_particle_data()
                        self.conn.send('D0:{};\r\n'.format(data_str).encode('ascii'))
                        continue
                    if CMD == 'S':
                        if len(params)==1:
                            try:
                                key, val = params[0].split('=')
                                if key == 'St_A':
                                    SA_P = float(val)
                                if key == 'Ns_A':
                                    DA_L = int(val)
                            except (ValueError, IndexError):
                                self.conn.send('Ee: not a valid SET parameter or SA_P or BUFFER_SIZE.\r\n'.encode('ascii'))
                                continue
                            tX = np.arange(0, DA_L*SA_P, SA_P)
                            DC_ERROR = dc_error(len(tX), amplitude=DC_ERROR_PARAMS[0], exponent=DC_ERROR_PARAMS[1])
                            self.conn.send('Ok.\r\n'.encode('ascii'))
                            print("reset tX")
                            continue
                        self.conn.send('Ee: not a valid SET command.\r\n'.encode('ascii'))
                        continue
                    if CMD == 'E':
                        self.conn.send('Ok: Quitting...\r\n'.encode('ascii'))
                        print('DAQ: conn close.')
                        self.conn.close()
                        time.sleep(0.2)
                        break
                    if CMD == 'C':
                        self.conn.send('Ok.\r\n'.encode('ascii'))
                        continue
                    if CMD == 'L':
                        self.conn.send('Ok.\r\n'.encode('ascii'))
                        continue
                    else:
                        self.conn.send('Ee: Do not understand.\r\n'.encode('ascii'))

    def conn_close(self):
        self.conn.close()



######################### Entry point

### This is the main thread; everything runs in sub-threads!
if __name__ == "__main__":
    start_signal = pyqtSignal()
    #start the threads; there aer 3! Timer, DAQServer and CNTserver

    print('#This is a hw-emulator server\n#It listens to commands at localhost:{:d}, and supports the following CMDs:\n'.format(TCP_PORT+1) + \
          '#\t`R %d`   - get particle signal in time domain Only "R 1" is implements!\n' + \
          '#\t`Q`      - query state\n' + \
          '#\t`S %f %d`- sampling_period and numer of samples\n' + \
          '#\t`E`      - close conn and exit\n' + \
          '#\tother    - ignored!\n')
    print('--------------- NOTE && ATTENTION: -------------------\n' + \
          'None so far!\n') 

    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('-R', '--rep_rate', type=float, default=1, help='Acqusition repetiotin rate in Hz')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print all the recieved commands')
    parser.add_argument('-p', '--particles', type=str, default="10", help="Resonant f0 of particles in Hz; coma separated, i.e., 100,150")
    parser.add_argument('-n', '--noiseADD', type=float, default=0, help="Random noise added to the signal")
    parser.add_argument('-N', '--noiseMUL', type=float, default=0, help="Random noise multipling the signal as: *(1+rnd(0,1)/100*noiseMUL)")
    parser.add_argument('-P', '--randPhase', action='store_true', help="Randomise phase for each sin function")
    parser.add_argument('--white_noise', type=float, default=0, help="amplitude of added white noise")
    parser.add_argument('--pink_noise', type=float, default=0, help="amplitude of added pink noise")
    parser.add_argument('--dc_error_A', type=float, default=0, help="dc_error amplitude (see noises.py)")
    parser.add_argument('--dc_error_E', type=float, default=1, help="dc_error exponent (see noises.py)")
    parser.add_argument('--one_shot', action='store_true', help='Generate the data only once and be done with it!')
    args = parser.parse_args()

    if args.verbose == True:
        VERBOSE = True

    RepRate = args.rep_rate if args.rep_rate > 0.1 else 1.0 

    ReqParticles = [ float(i) for i in args.particles.split(',') ]
    ReqNoiseADD = args.noiseADD
    ReqNoiseMUL = args.noiseMUL
    RandPhase = args.randPhase
    DC_ERROR_PARAMS = [args.dc_error_A, args.dc_error_E]
    NOISES_PARAMS[0] = args.white_noise
    NOISES_PARAMS[1] = args.pink_noise

    print('--------------- PARAMETERS:        -------------------')
    print("Particles: ", ReqParticles)
    print("Noise Additive: ", ReqNoiseADD, "Noise Multiplicative: ", ReqNoiseMUL, "\n")

    if args.one_shot:
        generate_particle_data(verbose=True)
        exit(0)


    app = QCoreApplication([])
    QLocale.setDefault(QLocale(QLocale.C))  #Set locale, so that we have decimal '.' not ',' !!

    DAQ = DAQ_thread_Class()
    threadDAQ = QThread()
    DAQ.moveToThread(threadDAQ)
    DAQ.worker_signal.connect(DAQ.worker)
    threadDAQ.start()


    #now I start the worker() in two threads that do the TCP!
    #CNT.worker_signal.emit()
    DAQ.worker_signal.emit()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app.exec_()
    #we shall never get here...

    DAQ.conn_close_signal.emit()
    time.sleep(0.1)
    DAQ.quit()

